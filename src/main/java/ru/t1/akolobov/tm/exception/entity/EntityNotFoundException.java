package ru.t1.akolobov.tm.exception.entity;

public class EntityNotFoundException extends AbstractEntityNotFoundException {

    public EntityNotFoundException() {
        super("Error! Entity not found...");
    }

}
