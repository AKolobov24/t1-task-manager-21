package ru.t1.akolobov.tm.exception.user;

public final class IncorrectLoginOrPasswordException extends AbstractUserException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Incorrect login or password...");
    }

}
