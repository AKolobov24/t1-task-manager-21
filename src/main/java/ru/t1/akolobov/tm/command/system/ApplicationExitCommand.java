package ru.t1.akolobov.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    public static final String NAME = "exit";

    public static final String DESCRIPTION = "Exit application.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
