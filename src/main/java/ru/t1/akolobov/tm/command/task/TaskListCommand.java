package ru.t1.akolobov.tm.command.task;

import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    public static final String NAME = "task-list";

    public static final String DESCRIPTION = "Display list of all tasks.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getAuthService().getUserId();
        final List<Task> taskList = getTaskService().findAll(userId, sort);
        int index = 1;
        for (final Task task : taskList) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
