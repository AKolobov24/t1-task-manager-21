package ru.t1.akolobov.tm.api.service;

import ru.t1.akolobov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    M add(String userId, M model);

    void clear(String userId);

    boolean existById(String userId, String id);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<? super M> comparator);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    Integer getSize(String userId);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

}
