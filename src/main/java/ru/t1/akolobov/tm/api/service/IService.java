package ru.t1.akolobov.tm.api.service;

import ru.t1.akolobov.tm.api.repository.IRepository;
import ru.t1.akolobov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
