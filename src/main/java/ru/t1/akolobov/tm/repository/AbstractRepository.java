package ru.t1.akolobov.tm.repository;

import ru.t1.akolobov.tm.api.repository.IRepository;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public boolean existById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator<? super M> comparator) {
        return models.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public M findOneById(final String id) {
        return models.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public Integer getSize() {
        return models.size();
    }

    @Override
    public M remove(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        final M model = Optional.ofNullable(findOneById(id))
                .orElseThrow(EntityNotFoundException::new);
        models.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M model = Optional.ofNullable(findOneByIndex(index))
                .orElseThrow(EntityNotFoundException::new);
        models.remove(model);
        return model;
    }

}
