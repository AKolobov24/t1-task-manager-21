package ru.t1.akolobov.tm.service;

import ru.t1.akolobov.tm.api.service.IAuthService;
import ru.t1.akolobov.tm.api.service.IUserService;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.exception.user.*;
import ru.t1.akolobov.tm.model.User;
import ru.t1.akolobov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = Optional.ofNullable(userService.findByLogin(login))
                .orElseThrow(IncorrectLoginOrPasswordException::new);
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        return Optional.ofNullable(userService.findOneById(userId))
                .orElseThrow(AccessDeniedException::new);
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = Optional.ofNullable(user.getRole())
                .orElseThrow(PermissionDeniedException::new);
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionDeniedException();
    }

}
